<?php

namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MemberController extends Controller
{
    public function index()
    {
    	return view('admin.member.list.index');
    }

    public function create()
    {
    	return view('admin.member.list.create');
    }
}
