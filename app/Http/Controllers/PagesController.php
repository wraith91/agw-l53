<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
	public function home()
	{
		return view('app.index');
	}

	public function promosi()
	{
		return view('app.promosi');
	}

	public function katalog()
	{
		return view('app.katalog');
	}

	public function bantuan()
	{
		return view('app.bantuan');
	}

	public function daftar()
	{
		return view('app.daftar');
	}

	public function login()
	{
		return view('app.login');
	}
}
