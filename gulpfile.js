const elixir = require('laravel-elixir');
require('laravel-elixir-imagemin');
require('laravel-elixir-sass-compass');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 // */
elixir.config.images = {
    folder: 'images',
    outputFolder: 'main/images'
};
elixir.config.testing.phpUnit.command = 'vendor/bin/phpunit --verbose --colors=always';

elixir(mix => {
    mix.compass(['app.scss', 'admin.scss'], 'public/main/css/', {
      sourcemap: false
    })

    .sass('grid.sass', 'public/main/css')

    .copy('resources/assets/fonts', 'public/main/fonts')
    .copy('resources/assets/components/materialize/fonts', 'public/main/fonts')

    .styles([
        'resources/assets/components/slick-carousel/slick/*.css',
        'resources/assets/components/aos/dist/aos.css',
    ], 'public/main/css/vendor.css', './')

    .scripts(
    [
        '../components/slick-carousel/slick/slick.js',
        '../components/aos/dist/aos.js',
        '../components/materialize/dist/js/materialize.js'
    ], 'public/main/js/all.js')

    .scripts('../components/materialize/dist/js/materialize.js', 'public/main/js/materialize.js')

    .imagemin({
      optimizationLevel: 3,
      progressive: true,
      interlaced: true
    });
});
