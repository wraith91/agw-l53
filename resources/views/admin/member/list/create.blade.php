@extends('layouts.admin.master')

@section('admin-header')
	<h2>Member List</h2>
@stop

@section('admin-content')
<div class="columns is-multiline">
	<div class="column is-12 panel">
		<ul class="action-list">
			<li><a class="waves-effect waves-light btn btn blue darken-3" href="/admin/members/list"><i class="material-icons left">arrow_back</i>Back</a></li>
		</ul>
	</div>
	<div class="column is-6 panel">
		<h5 class="block-title">Profile</h5>
		<div class="input-field">
			  {!! Form::label('system_id', 'System ID') !!}
			  {!! Form::text('system_id', null, ['class' => 'validate', 'placeholder' => '9bet ID of the Member']) !!}
		</div>
		<div class="input-field">
			  {!! Form::label('first_name', 'First Name') !!}
			  {!! Form::text('first_name', null, ['class' => 'validate']) !!}
		</div>
		<div class="input-field">
			  {!! Form::label('last_name', 'Last Name') !!}
			  {!! Form::text('last_name', null, ['class' => 'validate']) !!}
		</div>
		<div class="input-field">
			  {!! Form::label('mobile', 'Mobile') !!}
			  {!! Form::text('mobile', null, ['class' => 'validate']) !!}
		</div>
		<div class="input-field">
			  {!! Form::label('country', 'Country') !!}
			  {!! Form::text('country', null, ['class' => 'validate']) !!}
		</div>
		<div class="input-field">
			  {!! Form::label('address', 'Address') !!}
			  {!! Form::text('address', null, ['class' => 'validate']) !!}
		</div>
	</div>
	<div class="column is-6 panel">
		<h5 class="block-title">Account</h5>
		<div class="input-field">
			{!! Form::label('username', 'Username') !!}
			{!! Form::text('username', null, ['class' => 'validate', 'placeholder' => 'Username of the Member']) !!}
		</div>
		<div class="input-field">
			{!! Form::label('email', 'Email') !!}
			{!! Form::text('email', null, ['class' => 'validate', 'placeholder' => 'Valid email of the Member']) !!}
		</div>
		<div class="input-field">
			{!! Form::label('password', 'Password') !!}
			{!! Form::password('password', ['class' => 'validate']) !!}
		</div>
		<div class="input-field">
			{!! Form::label('password_confirmation', 'Password Confirmation') !!}
			{!! Form::password('password_confirmation', ['class' => 'validate']) !!}
		</div>
	</div>
	<div class="column is-12 panel">
		<ul class="action-list">
			<li>{!! Form::submit('Save', ['class' => 'waves-effect waves-light btn btn blue darken-3']) !!}</li>
		</ul>
	</div>
</div>

@stop