@extends('layouts.admin.master')

@section('admin-header')
	<h2>Member List</h2>
@stop

@section('admin-content')
<div class="columns is-multiline">
	<div class="column is-12 panel">
		<ul class="action-list">
			<li><a class="waves-effect waves-light btn blue darken-3" href="list/create"><i class="material-icons left">add</i>Create Member Account</a></li>
			{{-- <li><a class="waves-effect waves-light btn" href="{{ URL::route('admin::member:send:batch') }}"><i class="material-icons left">mail</i>Mail All</a></li> --}}
			<li class="last-item input-field">{!! Form::text('Search', null, ['class' => 'search', 'id' => 'search', 'placeholder' => 'Search for member']) !!}</li>
		</ul>
	</div>
	<div class="column is-12 panel">
		<table id="table" class="striped highlight responsive-table">
			<thead>
				<tr>
					<th data-field="index">No.</th>
					<th data-field="name">Name</th>
					<th data-field="username">Username</th>
					<th data-field="email">Email</th>
					<th data-field="system-id">System ID</th>
					<th data-field="total-points">Total Points</th>
					<th data-field="last-update">Last Update</th>
					<th data-field="action">Action</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>asdjkas</td>
					<td>asdjkas</td>
					<td>asdjkas</td>
					<td>asdjkas</td>
					<td>asdjkas</td>
					<td>asdjkas</td>
					<td>asdjkas</td>
					<td>asdjkas</td>
				</tr>
				<tr>
					<td>asdjkas</td>
					<td>asdjkas</td>
					<td>asdjkas</td>
					<td>asdjkas</td>
					<td>asdjkas</td>
					<td>asdjkas</td>
					<td>asdjkas</td>
					<td>asdjkas</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
@stop