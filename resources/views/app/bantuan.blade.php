@extends('layouts.app.master')

@section('contents')
<div class="static-page">
	<section class="static-header-block block" data-aos="fade-zoom-in">
		<div class="wrapper">
			<img src="{{ asset('main/images/bantuan-page/help-banner.png') }}" alt="">
		</div>
	</section>
	<section class="bantuan-block block" data-aos="fade-up">
		<div class="wrapper">
			<div class="bantuan-panel open">
				<h2 class="bantuan-title">
					Bagaimana cara untuk bergabung?
				</h2>
				<div class="bantuan-body">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium, perspiciatis, laboriosam. Perferendis placeat, ullam facere fuga id similique officiis nam! Ea architecto cumque quos dolorum, rem doloribus error. Officiis, dolor?
				</div>
			</div>
			<div class="bantuan-panel close">
				<h2 class="bantuan-title">
					Bagaimana cara untuk memilih hadiah?
				</h2>
				<div class="bantuan-body">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium, perspiciatis, laboriosam. Perferendis placeat, ullam facere fuga id similique officiis nam! Ea architecto cumque quos dolorum, rem doloribus error. Officiis, dolor?
				</div>
			</div>
			<div class="bantuan-panel close">
				<h2 class="bantuan-title">
					Bagaimana cara untuk mengganti password user?
				</h2>
				<div class="bantuan-body">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium, perspiciatis, laboriosam. Perferendis placeat, ullam facere fuga id similique officiis nam! Ea architecto cumque quos dolorum, rem doloribus error. Officiis, dolor?
				</div>
			</div>
			<div class="bantuan-panel close">
				<h2 class="bantuan-title">
					Bagaimana cara untuk mengganti email yang terdaftar?
				</h2>
				<div class="bantuan-body">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium, perspiciatis, laboriosam. Perferendis placeat, ullam facere fuga id similique officiis nam! Ea architecto cumque quos dolorum, rem doloribus error. Officiis, dolor?
				</div>
			</div>
			<div class="bantuan-panel close">
				<h2 class="bantuan-title">
					Bagaimana bila pengiriman hadiah ke alamat berbeda?
				</h2>
				<div class="bantuan-body">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium, perspiciatis, laboriosam. Perferendis placeat, ullam facere fuga id similique officiis nam! Ea architecto cumque quos dolorum, rem doloribus error. Officiis, dolor?
				</div>
			</div>
		</div>
	</section>
</div>
@stop

@section('footer')
<script>
	var accItem = document.getElementsByClassName('bantuan-panel');
	var accHD = document.getElementsByClassName('bantuan-title');

	for (i = 0; i < accHD.length; i++) {
		accHD[i].addEventListener('click', toggleItem, false);
	}
	function toggleItem() {
		var itemClass = this.parentNode.className;

		for (i = 0; i < accItem.length; i++) {
			accItem[i].className = 'bantuan-panel close';
		}
		if (itemClass == 'bantuan-panel close') {
			this.parentNode.className = 'bantuan-panel open';
		}
	}
</script>
@stop