@extends('layouts.app.master')

@section('contents')
<div class="static-page">
	<section class="static-header-block block">
		<div class="wrapper">
			<img src="{{ asset('main/images/daftar-page/daftar-banner.png') }}" alt="">
		</div>
	</section>
	<section class="winner-block block">
		<div class="wrapper">
			<div class="dropdown-panel">
				<select>
					<option value="1">Anniversary 1st</option>
				</select>
			</div>
			<div class="winner-board">
				<h3>Hadiah utama iPad Air</h3>
				<div class="winner-list">
					<ol class="single-list">
						<li>John</li>
						<li>Jane</li>
						<li>Bart</li>
						<li>Simon</li>
						<li>Clint</li>
					</ol>
				</div>
			</div>
			<div class="winner-board">
				<h3>25 Pemenang Free Credit 1jt</h3>
				<div class="winner-list">
					<ol>
						<li>John</li>
						<li>Jane</li>
						<li>Bart</li>
						<li>Simon</li>
						<li>Clint</li>
					</ol>
					<ol>
						<li>John</li>
						<li>Jane</li>
						<li>Bart</li>
						<li>Simon</li>
						<li>Clint</li>
					</ol>
					<ol>
						<li>John</li>
						<li>Jane</li>
						<li>Bart</li>
						<li>Simon</li>
						<li>Clint</li>
					</ol>
					<ol>
						<li>John</li>
						<li>Jane</li>
						<li>Bart</li>
						<li>Simon</li>
						<li>Clint</li>
					</ol>
					<ol>
						<li>John</li>
						<li>Jane</li>
						<li>Bart</li>
						<li>Simon</li>
						<li>Clint</li>
					</ol>
				</div>
			</div>
			<div class="winner-board">
				<h3>25 Pemenang Free Credit 500rb</h3>
				<div class="winner-list">
					<ol>
						<li>John</li>
						<li>Jane</li>
						<li>Bart</li>
						<li>Simon</li>
						<li>Clint</li>
					</ol>
					<ol>
						<li>John</li>
						<li>Jane</li>
						<li>Bart</li>
						<li>Simon</li>
						<li>Clint</li>
					</ol>
					<ol>
						<li>John</li>
						<li>Jane</li>
						<li>Bart</li>
						<li>Simon</li>
						<li>Clint</li>
					</ol>
					<ol>
						<li>John</li>
						<li>Jane</li>
						<li>Bart</li>
						<li>Simon</li>
						<li>Clint</li>
					</ol>
					<ol>
						<li>John</li>
						<li>Jane</li>
						<li>Bart</li>
						<li>Simon</li>
						<li>Clint</li>
					</ol>
				</div>
			</div>
		</div>
	</section>
</div>
@stop