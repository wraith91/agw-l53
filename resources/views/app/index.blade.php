@extends('layouts.app.master')

@section('contents')
<section class="slider-block">
    <ul class="rslides">
        <li><img src="{{ asset('main/images/slider/slider-1.jpg') }}" alt="Banner 1"></li>
    </ul>
</section>
<div class="bg-white">
    <div class="index-page">
    <section class="mc-block block">
        <div class="wrapper">
            <div class="mc-block-context" data-aos="fade-right" data-aos-offset="300">
                <h3>Kumpulkan Point Dan Tukarkan Point Anda Dengan Gadget Atau Barang Premium Lainnya</h3>
                <p>With Absolute Rewards it is easy to make every experience with use even more rewarding. You can earn on almost everything you enjoy at our properties including, dining, hotel stays, play, spa treatments cocktails or a coffee - just remember to use your Absolute Rewards card</p>
                <p>There are also plenty of ways to use your Absolute Dollars with Absolute Rewards, with a range of a fantastic products and experience to choose from including luxurious escapes, spa treatments and much more.</p>
            </div>
            <div class="mc-block-image">
                <img data-aos="fade-left" class="img1" src="{{ asset('main/images/index-page/mc-camera.png') }}" alt="Camera">
                <img data-aos="fade-left" data-aos-delay="250" class="img2" src="{{ asset('main/images/index-page/mc-girlright.png') }}" alt="">
                <img data-aos="fade-down" class="img3" src="{{ asset('main/images/index-page/mc-ipad.png') }}" alt="">
                <img data-aos="fade-right" data-aos-delay="250" class="img4" src="{{ asset('main/images/index-page/mc-girlleft.png') }}" alt="">
                <img data-aos="fade-up" class="img5" src="{{ asset('main/images/index-page/mc-iphone.png') }}" alt="">
                <img data-aos="fade-zoom-in" data-aos-delay="450" class="img6" src="{{ asset('main/images/index-page/mc-shadow.png') }}" alt="">
            </div>
        </div>
    </section>
    <section class="mc-tab-block block" data-aos="fade-zoom-in">
        <div class="wrapper animated fadeIn">
            <ul class="mc-tabs">
                <li class="mc-tab-item"><a href="javascript:void(0)" class="mc-tab-link active" onclick="openTab(event, 'Tab-1')">Cara Mendapatkan Point</a></li>
                <li class="mc-tab-item"><a href="javascript:void(0)" class="mc-tab-link" onclick="openTab(event, 'Tab-2')">Cara Menukarkan Point</a></li>
            </ul>
            <div id="Tab-1" class="mc-tab-content" style="display: flex;">
                <div class="tab-content-col">
                    <div class="tab-content-img">
                        <img src="{{ asset('main/images/index-page/tab-diamond.png') }}" class="tab-img" alt="Diamond">
                        <h4 class="blue-label">Jadi Member VIP</h4>
                    </div>
                    <div class="tab-content-label">
                        Program VIP kami menawarkan banyak manfaat bagi member, antara lain : Nilai konversi point yang lebih tinggi, serta promosi kejutan ekstra. Untuk bergabung menjadi member VIP, member 9bet hanya perlu melakukan deposit awal minimal sebesar Rp 10.000.000,-
                    </div>
                </div>
                <div class="tab-content-col">
                    <div class="tab-content-img">
                        <img src="{{ asset('main/images/index-page/tab-star.png') }}" class="tab-img" alt="Diamond">
                        <h4 class="blue-label">Setiap Turnover member VIP sebesar Rp 10.000.000,- akan otomatis mendapatkan 1 point.</h4>
                    </div>
                    <div class="tab-content-label">
                        Syarat turnover telah kami riset dengan seksama untuk memberikan keuntungan yang adil bagi kedua belah pihak, serta untuk menghindari terjadinya segala bentuk kecurangan demi kenyamanan kedua belah pihak.
                    </div>
                </div>
                <div class="tab-content-col">
                    <div class="tab-content-img">
                        <img src="{{ asset('main/images/index-page/tab-money.png') }}" class="tab-img" alt="Diamond">
                        <h4 class="blue-label">Setiap Cash Back sebesar Rp 1.000.000,- juga otomatis mendapatkan 1 point.</h4>
                    </div>
                    <div class="tab-content-label">
                        Kami memberikan keuntungan lainnya selain cash back, yakni berupa point bagi segenap member VIP yang bertaruh melalui perusahaan kami.
                    </div>
                </div>
            </div>
            <div id="Tab-2" class="mc-tab-content" style="display: none;">
                <div class="tab-content-col">
                    <div class="tab-content-img">
                        <img src="{{ asset('main/images/index-page/tab-book.png') }}" class="tab-img" alt="Diamond">
                        <h4 class="blue-label">Pilih Gift Anda</h4>
                    </div>
                    <div class="tab-content-label">
                        Masuk ke menu Katalog dan pilih gift sesuai selera anda berdasarkan point yang anda miliki.
                    </div>
                </div>
                <div class="tab-content-col">
                    <div class="tab-content-img">
                        <img src="{{ asset('main/images/index-page/tab-person.png') }}" class="tab-img" alt="Diamond">
                        <h4 class="blue-label">Informasi ke CS Kami</h4>
                    </div>
                    <div class="tab-content-label">
                        Setelah mengetahui gift yang akan anda tukarkan, informasikan nama product melalui livechat.
                    </div>
                </div>
                <div class="tab-content-col">
                    <div class="tab-content-img">
                        <img src="{{ asset('main/images/index-page/tab-gift.png') }}" class="tab-img" alt="Diamond">
                        <h4 class="blue-label">Pilih Gift Anda</h4>
                    </div>
                    <div class="tab-content-label">
                        Customer support kami akan mencatat pilihan gift anda. Dan akan dicek dengan jumlah point anda. Kemudian akan dikonfirmasi ke anda bila mencukupi, hadiah akan dikirimkan langsung ke alamat anda. Ketersediaan barang harap hubungi customer support kita terlebih dahulu.
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="mc-katalog-lists-block block">
        <ul class="katalog-lists">
            <li class="katalog-item" data-aos="fade-left">
                <div class="item-image"><img src="http://9betrewards.com/main/images/rewards/1473152131-Sony DSC W830 black.png" alt="#"></div>
                <div class="item-info">
                    <h4>Samsung</h4>
                    <h5>Samsung WB250F Kamera</h5>
                    <h5 class="item-value">1500 points</h5>
                </div>
                <div class="item-link">
                    <div class="item-button">
                        <a onclick="parent.LC_API.open_chat_window()">PILIH</a>
                    </div>
                </div>
            </li>
            <li class="katalog-item" data-aos="fade-left">
                <div class="item-image"><img src="http://9betrewards.com/main/images/rewards/1473152131-Sony DSC W830 black.png" alt="#"></div>
                <div class="item-info">
                    <h4>Samsung</h4>
                    <h5>Samsung WB250F Kamera</h5>
                    <h5 class="item-value">1500 points</h5>
                </div>
                <div class="item-link">
                    <div class="item-button">
                        <a onclick="parent.LC_API.open_chat_window()">PILIH</a>
                    </div>
                </div>
            </li>
            <li class="katalog-item" data-aos="fade-left">
                <div class="item-image"><img src="http://9betrewards.com/main/images/rewards/1473152131-Sony DSC W830 black.png" alt="#"></div>
                <div class="item-info">
                    <h4>Samsung</h4>
                    <h5>Samsung WB250F Kamera</h5>
                    <h5 class="item-value">1500 points</h5>
                </div>
                <div class="item-link">
                    <div class="item-button">
                        <a onclick="parent.LC_API.open_chat_window()">PILIH</a>
                    </div>
                </div>
            </li>
            <li class="katalog-item" data-aos="fade-left">
                <div class="item-image"><img src="http://9betrewards.com/main/images/rewards/1473152131-Sony DSC W830 black.png" alt="#"></div>
                <div class="item-info">
                    <h4>Samsung</h4>
                    <h5>Samsung WB250F Kamera</h5>
                    <h5 class="item-value">1500 points</h5>
                </div>
                <div class="item-link">
                    <div class="item-button">
                        <a onclick="parent.LC_API.open_chat_window()">PILIH</a>
                    </div>
                </div>
            </li>
            <li class="katalog-item" data-aos="fade-left">
                <div class="item-image"><img src="http://9betrewards.com/main/images/rewards/1473152131-Sony DSC W830 black.png" alt="#"></div>
                <div class="item-info">
                    <h4>Samsung</h4>
                    <h5>Samsung WB250F Kamera</h5>
                    <h5 class="item-value">1500 points</h5>
                </div>
                <div class="item-link">
                    <div class="item-button">
                        <a onclick="parent.LC_API.open_chat_window()">PILIH</a>
                    </div>
                </div>
            </li>
            <li class="katalog-item" data-aos="fade-left">
                <div class="item-image"><img src="http://9betrewards.com/main/images/rewards/1473152131-Sony DSC W830 black.png" alt="#"></div>
                <div class="item-info">
                    <h4>Samsung</h4>
                    <h5>Samsung WB250F Kamera</h5>
                    <h5 class="item-value">1500 points</h5>
                </div>
                <div class="item-link">
                    <div class="item-button">
                        <a onclick="parent.LC_API.open_chat_window()">PILIH</a>
                    </div>
                </div>
            </li>
            <li class="katalog-item" data-aos="fade-left">
                <div class="item-image"><img src="http://9betrewards.com/main/images/rewards/1473152131-Sony DSC W830 black.png" alt="#"></div>
                <div class="item-info">
                    <h4>Samsung</h4>
                    <h5>Samsung WB250F Kamera</h5>
                    <h5 class="item-value">1500 points</h5>
                </div>
                <div class="item-link">
                    <div class="item-button">
                        <a onclick="parent.LC_API.open_chat_window()">PILIH</a>
                    </div>
                </div>
            </li>
            <li class="katalog-item" data-aos="fade-left">
                <div class="item-image"><img src="http://9betrewards.com/main/images/rewards/1473152131-Sony DSC W830 black.png" alt="#"></div>
                <div class="item-info">
                    <h4>Samsung</h4>
                    <h5>Samsung WB250F Kamera</h5>
                    <h5 class="item-value">1500 points</h5>
                </div>
                <div class="item-link">
                    <div class="item-button">
                        <a onclick="parent.LC_API.open_chat_window()">PILIH</a>
                    </div>
                </div>
            </li>
            <li class="katalog-item" data-aos="fade-left">
                <div class="item-image"><img src="http://9betrewards.com/main/images/rewards/1473152131-Sony DSC W830 black.png" alt="#"></div>
                <div class="item-info">
                    <h4>Samsung</h4>
                    <h5>Samsung WB250F Kamera</h5>
                    <h5 class="item-value">1500 points</h5>
                </div>
                <div class="item-link">
                    <div class="item-button">
                        <a onclick="parent.LC_API.open_chat_window()">PILIH</a>
                    </div>
                </div>
            </li>
            <li class="katalog-item" data-aos="fade-left">
                <div class="item-image"><img src="http://9betrewards.com/main/images/rewards/1473152131-Sony DSC W830 black.png" alt="#"></div>
                <div class="item-info">
                    <h4>Samsung</h4>
                    <h5>Samsung WB250F Kamera</h5>
                    <h5 class="item-value">1500 points</h5>
                </div>
                <div class="item-link">
                    <div class="item-button">
                        <a onclick="parent.LC_API.open_chat_window()">PILIH</a>
                    </div>
                </div>
            </li>
        </ul>
    </section>
</div>
</div>
@stop

@section('footer')
<script>
	$(document).ready(function(){
		$('.katalog-lists').slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            centerMode: true,
            variableWidth: true,
            arrows: false,
            responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    infinite: true,
                    dots: false,
                }
            }]
		});
	});
</script>
@stop