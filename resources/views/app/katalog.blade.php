@extends('layouts.app.master')

@section('contents')
<div class="static-page">
	<section class="static-header-block block">
		<div class="wrapper">
			<h4 class="page-title">Katalog</h4>
		</div>
	</section>
	<section class="katalog-block block">
		<div class="wrapper">
			<div class="katalog-category">
				<ul>
					<li class="reset active-katalog">Semua Kategori</li>
					<li class="fashion">Fashion</li>
					<li class="watch">Jam Tangan</li>
					<li class="camera">Camera</li>
					<li class="mobile">Handphone</li>
					<li class="tablet">Tablet</li>
					<li class="perfume">Perfume</li>
					<li class="television">Television</li>
					<li class="accessory">Accessory</li>
				</ul>
			</div>
			<div class="initial-filter">
				<div class="filter-grid">
					<div class="hover-tint">
						<div class="hover-tint-button grid-button" id="fashion">
							LIHAT
						</div>
					</div>
					{{-- <img src="{{ asset('main/images/home/fashion.png')}}" alt="fashion"> --}}
					<img src="http://9betrewards.com/main/images/home/fashion.png" alt="fashion">
					<div class="product-desc-container">
						<div class="product-brand">
							Fashion
						</div>
					</div>
				</div>

				<div class="filter-grid">
					<div class="hover-tint">
						<div class="hover-tint-button grid-button" id="watch">
							LIHAT
						</div>
					</div>
					{{-- <img src="{{ asset('main/images/home/watch.png')}}" alt="watch"> --}}
					<img src="http://9betrewards.com/main/images/home/watch.png" alt="watch">
					<div class="product-desc-container">
						<div class="product-brand">
							Jam tangan
						</div>
					</div>
				</div>

				<div class="filter-grid">
					<div class="hover-tint">
						<div class="hover-tint-button grid-button" id="camera">
							LIHAT
						</div>
					</div>
					{{-- <img src="{{ asset('main/images/home/camera.png')}}" alt="camera"> --}}
					<img src="http://9betrewards.com/main/images/home/camera.png" alt="camera">
					<div class="product-desc-container">
						<div class="product-brand">
							Kamera
						</div>
					</div>
				</div>

				<div class="filter-grid">
					<div class="hover-tint">
						<div class="hover-tint-button grid-button" id="mobile">
							LIHAT
						</div>
					</div>
					{{-- <img src="{{ asset('main/images/home/dummy-1.png')}}" alt="phone"> --}}
					<img src="http://9betrewards.com/main/images/home/dummy-1.png" alt="phone">
					<div class="product-desc-container">
						<div class="product-brand">
							Handphone
						</div>
					</div>
				</div>

				<div class="filter-grid">
					<div class="hover-tint">
						<div class="hover-tint-button grid-button" id="tablet">
							LIHAT
						</div>
					</div>
					{{-- <img src="{{ asset('main/images/home/ipad.png')}}" alt=""> --}}
					<img src="http://9betrewards.com/main/images/home/ipad.png" alt="">
					<div class="product-desc-container">
						<div class="product-brand">
							Tablet
						</div>
					</div>
				</div>

				<div class="filter-grid">
					<a href="#watch"></a>
					<div class="hover-tint">
						<div class="hover-tint-button grid-button" id="perfume">
							LIHAT
						</div>
					</div>
					{{-- <img src="{{ asset('main/images/home/perfume.png')}}" alt="perfume"> --}}
					<img src="http://9betrewards.com/main/images/home/perfume.png" alt="perfume">
					<div class="product-desc-container">
						<div class="product-brand">
							Perfume
						</div>
					</div>
				</div>

				<div class="filter-grid">
					<div class="hover-tint">
						<div class="hover-tint-button grid-button" id="television">
							LIHAT
						</div>
					</div>
					{{-- <img src="{{ asset('main/images/home/tv.png')}}" alt=""> --}}
					<img src="http://9betrewards.com/main/images/home/tv.png" alt="">
					<div class="product-desc-container">
						<div class="product-brand">
							Television
						</div>
					</div>
				</div>

				<div class="filter-grid">
					<div class="hover-tint">
						<div class="hover-tint-button grid-button" id="accessory">
							LIHAT
						</div>
					</div>
					{{-- <img src="{{ asset('main/images/home/accessory.png')}}" alt=""> --}}
					<img src="http://9betrewards.com/main/images/home/accessory.png" alt="">
					<div class="product-desc-container">
						<div class="product-brand">
							Accessory
						</div>
					</div>
				</div>
			</div>

			<div class="katalog-products-container">
				<div class="katalog-products katalog-camera">
					<div class="hover-tint">
						<div class="hover-tint-button">
							<a onclick="parent.LC_API.open_chat_window()">PILIH</a>
						</div>
					</div>
					<img src="http://9bet.dev/main/images/rewards/1473152131-Sony DSC W830 black.png" alt=""/>
					<div class="product-desc-container">
						<div class="product-brand">
						Sony
						</div>
						<div class="product-name">
						DSC W830 Black
						</div>
						<div class="product-point">
						182 Points
						</div>
					</div>
				</div>
				<div class="katalog-products katalog-camera">
					<div class="hover-tint">
						<div class="hover-tint-button">
							<a onclick="parent.LC_API.open_chat_window()">PILIH</a>
						</div>
					</div>
					<img src="http://9bet.dev/main/images/rewards/1473152131-Sony DSC W830 black.png" alt=""/>
					<div class="product-desc-container">
						<div class="product-brand">
						Sony
						</div>
						<div class="product-name">
						DSC W830 Black
						</div>
						<div class="product-point">
						182 Points
						</div>
					</div>
				</div>
				<div class="katalog-products katalog-camera">
					<div class="hover-tint">
						<div class="hover-tint-button">
							<a onclick="parent.LC_API.open_chat_window()">PILIH</a>
						</div>
					</div>
					<img src="http://9bet.dev/main/images/rewards/1473152131-Sony DSC W830 black.png" alt=""/>
					<div class="product-desc-container">
						<div class="product-brand">
						Sony
						</div>
						<div class="product-name">
						DSC W830 Black
						</div>
						<div class="product-point">
						182 Points
						</div>
					</div>
				</div>
				<div class="katalog-products katalog-camera">
					<div class="hover-tint">
						<div class="hover-tint-button">
							<a onclick="parent.LC_API.open_chat_window()">PILIH</a>
						</div>
					</div>
					<img src="http://9bet.dev/main/images/rewards/1473152131-Sony DSC W830 black.png" alt=""/>
					<div class="product-desc-container">
						<div class="product-brand">
						Sony
						</div>
						<div class="product-name">
						DSC W830 Black
						</div>
						<div class="product-point">
						182 Points
						</div>
					</div>
				</div>
			</div>

			<div class="katalog-disclaimer">
				NOTE: Semua rewards akan dikirim melalui toko online.
				<br>
				Kami tidak berafiliasi dengan toko online manapun.
				<br>
				Keamanan dan kenyamanan member adalah perhatian utama kami.
			</div>
		</div>
	</section>
</div>
@stop

@section('footer')
<script>
//katalog
var katalogChoice = $('div.katalog-category > ul > li');
var katalogProducts = $('.katalog-products');
var katalogHeight = $('.katalog-products-container');
var katalogPagination = $('.katalog-pagination > ul > li');
var gridButton = $('.grid-button');
var initFilter = $('.initial-filter');

//init product position
if ($(window).width() > 800){
  $(document).ready(function(){
    var xspace = 0;
    var yspace = 0;
    var heightMultiplier = 0;
    var productLength = katalogProducts.length;
    console.log(productLength);

    for (var i = 0; i < productLength; i++) {
      katalogProducts.eq(i).css({
         transform: 'translateX('+xspace+'px) translateY('+yspace+'px)',
      });
      xspace = xspace + 240;
      if (xspace > 950) {
        xspace = 0;
        yspace = yspace + 280;
        heightMultiplier = heightMultiplier + 1;
      }
    }
    katalogHeight.css({
       height: heightMultiplier * 300,
       display: 'block'
    })
  })

  katalogChoice.on('click', function(){
    katalogChoice.removeClass('active-katalog');
    $(this).addClass('active-katalog');
    var choiceId = $(this).attr('class').split(' ')[0];
    console.log(choiceId);
    katalogProducts.addClass('jquery-hide');
    $('.katalog-' + choiceId).removeClass('jquery-hide');
    var heightMultiplier = 1;
    var xspace = 0;
    var yspace = 0;
    for (var i = 0; i < $('.katalog-' + choiceId).length; i++) {
       $('.katalog-' + choiceId).eq(i).css({
          transform: 'translateX('+xspace+'px) translateY('+yspace+'px)',
       });
       xspace = xspace + 240;
       if (xspace > 950) {
          xspace = 0;
          yspace = yspace + 280;
          heightMultiplier = heightMultiplier + 1;
       }
    }
    katalogHeight.css({
       height: heightMultiplier * 300,
       position: 'relative',
       opacity: 1,
       display: 'block'
    })
    $('.initial-filter').css({
       opacity: 0,
       position: 'absolute',
       zIndex: -99,
       width: '1000px'
    })
  })
  //new filter function
  gridButton.on('click', function(){
    var choiceId = $(this).attr('id');
    console.log(choiceId);

    katalogChoice.removeClass('active-katalog');
    $('.'+ choiceId).addClass('active-katalog');

    katalogProducts.addClass('jquery-hide');
    $('.katalog-' + choiceId).removeClass('jquery-hide');
    var heightMultiplier = 1;
    var xspace = 0;
    var yspace = 0;
    for (var i = 0; i < $('.katalog-' + choiceId).length; i++) {
       $('.katalog-' + choiceId).eq(i).css({
          transform: 'translateX('+xspace+'px) translateY('+yspace+'px)',
       });
       xspace = xspace + 240;
       if (xspace > 950) {
          xspace = 0;
          yspace = yspace + 280;
          heightMultiplier = heightMultiplier + 1;
       }
    }
    katalogHeight.css({
       height: heightMultiplier * 300,
       position: 'relative',
       opacity: 1,
       display: 'block'
    })
    $('.initial-filter').css({
      display: 'none',
    })
  })


  $('.reset').on('click', function(){
    // katalogProducts.removeClass('jquery-hide');
    // var xspace = 0;
    // var yspace = 0;
    // var heightMultiplier = 0;
    // var productLength = katalogProducts.length;
    // console.log(productLength);
    //
    // for (var i = 0; i < productLength; i++) {
    //   katalogProducts.eq(i).css({
    //      transform: 'translateX('+xspace+'px) translateY('+yspace+'px)',
    //   });
    //   xspace = xspace + 240;
    //   if (xspace > 950) {
    //     xspace = 0;
    //     yspace = yspace + 280;
    //     heightMultiplier = heightMultiplier + 1;
    //   }
    // }
    katalogHeight.css({
       height: 0,
       display: 'none',
    })
    $('.initial-filter').css({
      opacity: 1,
      position: 'relative',
      zIndex: 1,
      // width: '1000px'
      display: 'block',
    })
  })

  katalogPagination.on('click', function(){
    katalogPagination.removeClass('page-active');
    $(this).addClass('page-active');
  })
}
</script>
@stop