@extends('layouts.app.master')

@section('contents')
<div class="static-page login-page">
	<section class="login-block block">
		{!! Form::open([]) !!}
		<div class="wrapper">
			<div class="form-heading centered">Member Login</div>
			<div class="frm-group">
				<!-- username Field -->
				{!! Form::text('username', null, ['class' => 'frm-input', 'placeholder' => 'Username']) !!}
			</div>
			<div class="frm-group">
				<!-- password Field -->
				<div class="tooltip">?
					<span class="tooltiptext">Klik disini <span class="blue-label">untuk reset password</span></span>
				</div>
				{!! Form::password('password', ['class' => 'frm-input', 'placeholder' => 'Password']) !!}
			</div>
			<div class="frm-group">
				{!! Form::submit('Log In', ['class' => 'submit-btn']) !!}
			</div>
			<!-- <div class="forgot-password">
				Belum punya akun? <a href="#">Daftar sekarang!</a>
			</div> -->
		</div>
		{!! Form::close() !!}
	</section>
</div>
@stop