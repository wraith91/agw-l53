@extends('layouts.app.master')

@section('contents')
<div class="static-page">
	<section class="static-header-block block" data-aos="fade-zoom-in">
		<div class="wrapper">
			<img src="{{ asset('main/images/promosi-page/promosi-banner.png') }}" alt="">
		</div>
	</section>
	<section class="static-promo-block block">
		<div class="wrapper">
			<div class="promo-block promo1" data-aos="fade-up" data-aos-delay="200">
				<div class="promo-percentage">
					<h2>10</h2>
					<small>%</small>
				</div>
				<div class="promo-info">
					<h4>Bonus Pendaftaran</h4>
				</div>
				<a href="#" class="info-btn">Learn More</a>
			</div>
			<div class="promo-block promo2" data-aos="fade-up" data-aos-delay="500">
				<div class="promo-percentage">
					<h2>5</h2>
					<small>%</small>
				</div>
				<div class="promo-info">
					<h4>Cashback</h4>
				</div>
				<a href="#" class="info-btn">Learn More</a>
			</div>
			<div class="promo-block promo3" data-aos="fade-up" data-aos-delay="900">
				<div class="promo-percentage">
					<h2>0.9</h2>
					<small>%</small>
				</div>
				<div class="promo-info">
					<h4>Komisi Member VIP</h4>
				</div>
				<a href="#" class="info-btn">Learn More</a>
			</div>
			<div class="promo-block promo4" data-aos="fade-up" data-aos-delay="1100">
				<div class="promo-percentage">
					<h2>0.8</h2>
					<small>%</small>
				</div>
				<div class="promo-info">
					<h4>Komisi Casino</h4>
				</div>
				<a href="#" class="info-btn">Learn More</a>
			</div>
		</div>
	</section>
</div>
@stop