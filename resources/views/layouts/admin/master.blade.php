<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>AGW Admin Panel</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->
        <link rel="stylesheet" href="/main/css/admin.css">
		<link rel="stylesheet" href="/main/css/grid.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    </head>
    <body>
        <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->
        <div class="columns is-fullwidth is-gapless is-multiline" style="height: 100%">
            <aside class="column is-2">
                @include('layouts.admin.menu')
            </aside>
            <div class="column is-10 content ">
                <header>
                    @yield('admin-header')
                </header>
                <main>
                    {{-- @include('errors.list') --}}
                    {{-- @include('partials.flash') --}}
                    @yield('admin-content')
                </main>
            </div>
        </div>

		<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
        <script src="{{ asset('main/js/materialize.js') }}"></script>
    </body>
</html>