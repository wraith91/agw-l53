<nav>
    <h5 class="menu-block-greeting">
        <strong>{{'Hello ' . 'Admin' }}!</strong>
    </h5>
    <div class="menu-block">
        <h5 class="menu-block-title">
            My Account
        </h5>
        <ul>
            <li class="{{ set_active(['admin/account/dashboard']) }}">
                <a href="{{ URL::to('admin/account/dashboard') }}">Dashboard</a>
            </li>
            <li class="{{ set_active(['admin/account/settings']) }}">
                <a href="{{ URL::to('admin/account/settings') }}">Settings</a>
            </li>
            <li><a href="{{ URL::to('logout') }}">Logout</a></li>
        </ul>
    </div>
    <div class="menu-block">
        <h5 class="menu-block-title">
            Member
        </h5>
        <ul>
            <li class="{{ set_active(['admin/members/list', 'admin/members/list/*']) }}">
                <a href="{{ URL::to('admin/members/list') }}">List</a>
            </li>
            <li class="{{ set_active(['admin/members/point', 'admin/members/point/*']) }}">
                <a href="{{ URL::to('admin/members/point') }}">Pointing</a>
            </li>
            <li class="{{ set_active(['admin/members/redeem', 'admin/members/redeem/*']) }}">
                <a href="{{ URL::to('admin/members/redeem') }}">Redeeming</a>
            </li>
        </ul>
    </div>
</nav>