 {{-- Customer Support Block --}}
<section class="customer-support-block block">
    <div class="wrapper">
        <h4 class="blue-label">
            Customer Support
        </h4>
        <p class="centered">Untuk penjelasan lebih lanjut silakan hubungi Tim Customer Support kami kami yang berkualitas dan siap membantu Anda kapan saja melalui fitur Live Chat yang telah kami sediakan.</p>
        <p><em>Jadi tunggu apalagi? Segera bergabung dengan kami!</em></p>
        <ul class="contact-lists">
            <li class="contact-item">
                CS Manila - call Only
              <div class="blue-label">
                  +63 9163 688 999
              </div>
            </li>
            <li class="contact-item">
                sms indo - sms only
                <div class="blue-label">
                    +62 8211 811 9119
                </div>
            </li>
            <li class="contact-item">
              <img src="{{ asset('main/images/common-page/wechat.png') }}" alt="" />
              <span class="blue-label">Sembilanbet</span>
            </li>
            <li class="contact-item">
              <img src="{{ asset('main/images/common-page/ym.png') }}" alt="" />
              <span class="blue-label">cs9bet</span>
            </li>
            <li class="contact-item">
              <img src="{{ asset('main/images/common-page/bbm.png') }}" alt="" />
              <span class="blue-label">2B364851</span>
            </li>
        </ul>
    </div>
</section>
{{-- End of Customer Support Block --}}

<div class="divider"></div>

{{-- Bank Merchants Block --}}
<section class="bank-merchants-block block">
    <div class="wrapper">
        <h4>Kami menerima pembayaran melalui</h4>
        <img src="{{ asset('main/images/common-page/mc-bank-logos.png') }}" alt="Bank Logos" />
    </div>
</section>
{{-- End of Bank Merchants Block --}}

<div class="divider"></div>

{{-- Sub Navigation Block --}}
<section class="sub-nav-block block">
    <div class="wrapper">
        <ul>
            <li>
                <a href="#" onclick="MM_openBrWindow('http://www.9bet.me/body/main.php?content=termsConditions','9bet','scrollbars=yes,width=800,height=600')">terms &amp; conditions</a>
            </li>
            <li>
                <a href="#" onclick="MM_openBrWindow('http://www.9bet.me/body/main.php?content=privacyPolicy','9bet','scrollbars=yes,width=800,height=600')">privacy policy</a>
            </li>
            <li>
                <a href="#" onclick="MM_openBrWindow('http://www.9bet.me/body/main.php?content=responsibleGaming','9bet','scrollbars=yes,width=800,height=600')">responsible gaming</a>
            </li>
            <li>
                <a href="http://wap.9bet.com/">wap.9bet.com</a>
            </li>
            <li>
                <a href="http://smart.9bet.com/">smart.9bet.com</a>
            </li>
        </ul>
        <h5 class="copyright">Hak Cipta &copy; 2016 AGWGROUP</h5>
    </div>
</section>
{{-- End of Sub Navigation Block --}}