{{-- Top Bar Block --}}
<div class="top-block block">
    <div class="wrapper">
        <h4>Klik Disini Untuk Mengakses Website</h4>
        <a href="http://www.agenwin.com" target="_blank">
            <img src="{{ asset('main/images/index-page/agenwin-logo.png') }}" alt="Agenwin Logo">
        </a>
    </div>
</div>
{{-- End of Top Bar Block --}}

{{-- Navigation Block --}}
<div class="nav-block block">
    <div class="wrapper">
        <div class="logo">
            <a href="/">
                <img src="{{ asset('main/images/index-page/agw-logo.png') }}" alt="Agw Logo">
            </a>
        </div>
        <nav>
            <ul class="topnav" id="main-nav">
                <li class="home"><a href="/">Home</a></li>
                <li class="{{ set_active('katalog') }}"><a href="/katalog">Katalog</a></li>
                <li class="{{ set_active('promosi') }}"><a href="/promosi">Promosi</a></li>
                <li class="{{ set_active('daftar') }}"><a href="/daftar">Daftar Pemenang</a></li>
                <li class="{{ set_active('bantuan') }}"><a href="/bantuan">Bantuan</a></li>
                <li class="{{ set_active('auth/login') }}"><a href="/auth/login">Login</a></li>
                <li class="icon"><a href="javascript:void(0);" style="font-size:15px;" onclick="responsiveNav()">☰</a></li>
            </ul>
        </nav>
    </div>
</div>
{{-- End of Navigation Block --}}