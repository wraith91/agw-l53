<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Agenwin Rewards</title>
        <meta name="description" content="Agenwin Reward Site">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="{{ asset('main/css/app.css')}}">
        <link rel="stylesheet" href="{{ asset('main/css/vendor.css')}}">

        @yield('header')
    </head>
    <body>
        <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->
        <div class="container">
            <header>
                @include('layouts.app.header')
            </header>

            <main class="contents">
                @yield('contents')
            </main>

            <footer>
                @include('layouts.app.footer')
            </footer>
        </div>

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/ResponsiveSlides.js/1.53/responsiveslides.min.js"></script>
        <script src="{{ asset('main/js/all.js') }}"></script>

        @yield('footer')

        <script>
            AOS.init({
              offset: 200,
              duration: 900,
              easing: 'ease-in-sine',
              delay: 100,
              disable: function () {
                var maxWidth = 1049;
                return window.innerWidth < maxWidth;
              }
            });
            // if ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            //     $(document).ready(function() {

            //     });
            // } else {

            // }
        </script>
        <script>
            $(function() {
                $(".rslides").responsiveSlides({
                    speed: 1000,
                    pager: true,
                });
            });
        </script>
        <script>
            function responsiveNav() {
                var x = document.getElementById("main-nav");

                if (x.className === "topnav") {
                    x.className += " responsive";
                } else {
                    x.className = "topnav";
                }
            }
        </script>
        <script>
            function openTab(evt, id) {
                // Declare all variables
                var i, tabcontent, tablinks;

                // Get all elements with class="tabcontent" and hide them
                tabcontent = document.getElementsByClassName("mc-tab-content");
                for (i = 0; i < tabcontent.length; i++) {
                    tabcontent[i].style.display = "none";
                }

                // Get all elements with class="tablinks" and remove the class "active"
                tablinks = document.getElementsByClassName("mc-tab-link");
                for (i = 0; i < tablinks.length; i++) {
                    tablinks[i].className = tablinks[i].className.replace(" active", "");
                }

                // Show the current tab, and add an "active" class to the link that opened the tab
                document.getElementById(id).style.display = "flex";
                evt.currentTarget.className += " active";
            }
        </script>
        {{-- <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
 --}}
        <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
        {{-- <script>
            window.ga=function(){ga.q.push(arguments)};ga.q=[];ga.l=+new Date;
            ga('create','UA-XXXXX-Y','auto');ga('send','pageview')
        </script>
        <script src="https://www.google-analytics.com/analytics.js" async defer></script> --}}
    </body>
</html>