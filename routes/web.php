<?php

Route::get('/', 'PagesController@home');
Route::get('/promosi', 'PagesController@promosi');
Route::get('/katalog', 'PagesController@katalog');
Route::get('/bantuan', 'PagesController@bantuan');
Route::get('/daftar', 'PagesController@daftar');
Route::get('/auth/login', 'PagesController@login');

Route::get('/admin/account/dashboard', function() {
	return view('admin.dashboard');
});

Route::resource('/admin/members/list', 'Member\MemberController');